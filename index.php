<?php
$X = 6;
//$A = [6,6,1,8,2,3,6];
for($i=10000;$i>0;$i--) {
    $A[] = rand(0,3);
}
function solution(int $X, array $A) {
    $length = count($A);

    $a = array_slice($A,0,1);
    $b = array_slice($A,1);

    $aa = array_slice($A,0,-1);
    $bb = [end($A)];

    for($n = 1; $length>$n; $n++) {
        $find_in_a = array_count_values($a)[$X];
        $count_b = count($b);
        $not_find_in_b = $count_b - array_count_values($b)[$X];

        $find_in_aa = array_count_values($aa)[$X];
        $count_bb = count($bb);
        $not_find_in_bb = $count_bb - array_count_values($bb)[$X];

        if ($find_in_a == $not_find_in_b) {
            $solution = $n;
            echo "Итерация $n; Найдено - $find_in_a, не найдено - $not_find_in_b; <br> ";
            echo "Решение = $solution <br>";
            return $solution;
        }
        if ($find_in_aa == $not_find_in_bb) {
            $solution = count($A) - $n;
            echo "Итерация $n; Найдено - $find_in_aa, не найдено - $not_find_in_bb; <br> ";
            echo "Решение = $solution <br>";
            return $solution;
        }


        $a[] = array_shift($b);
        array_unshift($bb, array_pop($aa));

    }

    echo "Решений не найдено <br> ";
    return -1;

}

$start = microtime(true);
solution($X, $A);
echo 'Время выполнения скрипта: '.(microtime(true) - $start).' сек.';

